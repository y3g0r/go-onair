package providers

import (
	"gitlab.com/y3g0r/go-onair/providers/crawler"
	"gitlab.com/y3g0r/go-onair/providers/radiovolna"
)

var (
	Stations map[string]crawler.Interface
)

func init() {
	Stations = make(map[string]crawler.Interface)
	for stationName, provider := range radiovolna.Stations {
		Stations[stationName] = provider
	}
}
