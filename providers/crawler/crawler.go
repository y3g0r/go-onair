package crawler

import "time"

type AirInfo struct {
	Provider string
	Radio    string
	Stream   string
	Region   string
	FM       string
	Time     time.Time
	Artists  []string
	Song     string
	Stats    struct {
		ResponseTime      time.Duration
		ResponseSizeBytes int
	}
}

type Interface interface {
	Crawl() (*AirInfo, error)
	Toggle() bool
	Disabled() bool
}
