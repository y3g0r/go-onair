package radiovolna

import (
	"encoding/json"
	"errors"
	"fmt"
	"gitlab.com/y3g0r/go-onair/providers/crawler"
	"io/ioutil"
	"net/http"
	"net/url"
	"strconv"
	"time"
)

type cr struct {
	radio  string
	stream string
	region string
	fm     string
	off    bool

	streamId   int
	streamUrl  string
	nodeParser int
}

type radioVolnaAirInfo struct {
	Status int
	Data   struct {
		Status  string
		Message string
		Song    string
		Artist  string
	}
	Exec float64
}

func (c *cr) Crawl() (*crawler.AirInfo, error) {
	start := time.Now()
	resp, err := http.PostForm("https://parser.radiovolna.net/stations/last",
		url.Values{
			"streamId":   {strconv.Itoa(c.streamId)},
			"streamUrl":  {c.streamUrl},
			"nodeParser": {strconv.Itoa(c.nodeParser)},
		})
	if err != nil {
		return nil, errors.New(fmt.Sprintf("Error posting form: %v", err))
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	totalTime := time.Since(start)
	radioVolnaAir := new(radioVolnaAirInfo)
	err = json.Unmarshal(body, radioVolnaAir)
	if err != nil {
		return nil, err
	}
	return &crawler.AirInfo{
		Provider: "radiovolna.net",
		Radio:    c.radio,
		Stream:   c.stream,
		Region:   c.region,
		FM:       c.fm,
		Time:     time.Now(),
		Artists:  []string{radioVolnaAir.Data.Artist},
		Song:     radioVolnaAir.Data.Song,
		Stats: struct {
			ResponseTime      time.Duration
			ResponseSizeBytes int
		}{
			totalTime,
			len(body),
		},
	}, nil
}

func (c *cr) Toggle() bool {
	c.off = !c.off
	return !c.off
}

func (c *cr) Disabled() bool {
	return c.off
}

var (
	Stations map[string]crawler.Interface
)

func init() {
	Stations = make(map[string]crawler.Interface)
	Stations["Dj FM"] = &cr{radio: "Dj FM", region: "Украина, Киев", fm: "96.8", streamId: 8, streamUrl: "http://media2.brg.ua:8000/djfm_h"}
	Stations["NRJ - Energy"] = &cr{radio: "NRJ", stream: "Energy", region: "Украина, Киев", fm: "92.8", streamId: 11, streamUrl: "https://cast.radiogroup.com.ua/nrj"}
	Stations["Закарпаття FM"] = &cr{radio: "Закарпаття FM", region: "Украина, Мукачево", fm: "101.9", streamId: 37, streamUrl: "http://195.234.148.51:8000/;stream.nsv"}
	Stations["Закарпаття FM"] = &cr{radio: "ХIT FM", region: "Украина, Киев", fm: "96.4", streamId: 15, streamUrl: "https://online.hitfm.ua/HitFM"}
	Stations["Шансон"] = &cr{radio: "Шансон", region: "Украина, Киев", fm: "101.9", streamId: 24, streamUrl: "https://cast.fex.net/shanson_l"}
	Stations["Світ FM"] = &cr{radio: "Світ FM", region: "Украина, Ужгород", fm: "104.7", streamId: 55, streamUrl: "http://195.234.148.52:8000/stream.mp3"}
}
