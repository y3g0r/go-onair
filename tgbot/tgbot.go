package tgbot

import (
	"encoding/json"
	"fmt"
	"github.com/go-telegram-bot-api/telegram-bot-api"
	"gitlab.com/y3g0r/go-onair/db"
	"gitlab.com/y3g0r/go-onair/providers"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strconv"
	"strings"
)

const (
	options = `/notify песня - что бы получать оповещение когда песня в эфире
/list - что бы получить список отслеживаемых песен
/stop песня - что бы перестать отслеживать песню
/radios - что бы посмотреть список радио станций
/filter текст - что бы отфильтровать нотификации которые тебе не интересны
/filters - что бы посмотреть текущие фильтры
/removefilter текст - убрать фильтр`
	helpMsg        = "Привет!\n" + options + "\nВперед!"
	unknownCommand = "Сори, я этого не понял🤔 Попробуй:\n" + options
	notifyHelp     = "Не пойдет. Используй /notify pattern"
	notifySuccess  = "Окей! Дам знать когда %q будет в эфире. Если надоест используй /stop или /filter."
	listEmpty      = "Пока пусто🤔 Используй /notify pattern"
	stopHelp       = "Не пойдет. Используй /stop pattern"
	stopSuccess    = "Ok, больше не потревожим с %q 😉"
)

func SendMessage(to int, message string) {
	bot, err := tgbotapi.NewBotAPI(os.Getenv("TELEGA"))
	if err != nil {
		fmt.Println(err)
		return
	}
	msg := tgbotapi.NewMessage(int64(to), message)
	bot.Send(msg)
}

func TelegramWebhookHander(_ http.ResponseWriter, r *http.Request) {
	defer r.Body.Close()
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		log.Printf("%v\n", err)
		return
	}
	var update tgbotapi.Update
	err = json.Unmarshal(body, &update)
	if err != nil {
		log.Printf("%v\n", err)
		return
	}
	if update.Message == nil {
		return
	}
	if update.Message.From == nil {
		return
	}
	fmt.Printf("%d: %s\n", update.Message.From.ID, update.Message.Text)

	user := update.Message.From.ID
	text := update.Message.Text
	if len(text) == 0 {
		go SendMessage(update.Message.From.ID, helpMsg)
		return
	}
	components := strings.Split(text, " ")
	command := components[0]
	switch {
	case command == "/notify":
		if len(components) < 2 {
			go SendMessage(update.Message.From.ID, fmt.Sprintf(notifyHelp))
		} else {
			pattern := strings.Join(components[1:], " ")
			db.AddNotification(user, pattern)
			go SendMessage(user, fmt.Sprintf(notifySuccess, pattern))
		}
	case command == "/list":
		notifications := db.GetNotifications(user)
		builder := strings.Builder{}
		for i, n := range notifications {
			builder.WriteString(fmt.Sprintf("%d. %s\n", i+1, n))
		}
		if builder.Len() == 0 {
			builder.WriteString(listEmpty)
		}
		go SendMessage(user, builder.String())
	case command == "/stop":
		if len(components) < 2 {
			go SendMessage(update.Message.From.ID, fmt.Sprintf(stopHelp))
		} else {
			pattern := strings.Join(components[1:], " ")
			db.RemoveNotification(user, pattern)
			go SendMessage(user, fmt.Sprintf(stopSuccess, pattern))
		}
	case command == "/radios":
		listRadios(user)
	case command == "/filter":
	case command == "/filters":
	case command == "/removefilter":
	case command == "/admintoggle":
		adminid, err := strconv.Atoi(os.Getenv("ADMIN"))
		if err == nil {
			if user == adminid {
				radio := strings.Join(components[1:], " ")
				provider, ok := providers.Stations[radio]
				if !ok {
					go SendMessage(user, fmt.Sprintf("Не могу найти станцию %q", radio))
					return
				}
				enabled := provider.Toggle()
				if enabled {
					go SendMessage(user, fmt.Sprintf("Станция %q включена!", radio))
				} else {
					go SendMessage(user, fmt.Sprintf("Cтанция %q выключена", radio))
				}
			}
		}

	default:
		go SendMessage(update.Message.From.ID, unknownCommand)
	}
}

func listRadios(user int) {
	var admin bool
	adminid, err := strconv.Atoi(os.Getenv("ADMIN"))
	if err != nil {
		admin = false
	}
	if user == adminid {
		admin = true
	}
	radios := ""
	i := 1
	for radio, provider := range providers.Stations {
		if !admin {
			if provider.Disabled() {
				continue
			}
			radios += fmt.Sprintf("%d. %s\n", i, radio)
		} else {
			radios += fmt.Sprintf("%d. %s", i, radio)
			if provider.Disabled() {
				radios += " (отключенa)"
			}
			radios += "\n"
		}
		i++
	}
	if radios == "" {
		radios = "Пока ни одной станции, но мы работаем над этим."
	}
	go SendMessage(user, radios)
}
