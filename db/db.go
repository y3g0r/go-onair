package db

import (
	"strings"
	"sync"
)

type userPreferences struct {
	notifications map[string]bool
	filters       map[string]bool
}

type PrefDB struct {
	sync.Mutex
	m map[int]userPreferences
}

var db PrefDB

func InitDB() {
	db = PrefDB{}
	db.m = make(map[int]userPreferences)
}

func AddNotification(user int, pattern string) {
	db.Lock()
	defer db.Unlock()
	if _, ok := db.m[user]; !ok {
		db.m[user] = userPreferences{map[string]bool{}, map[string]bool{}}
	}
	pattern = strings.ToLower(pattern)
	db.m[user].notifications[pattern] = true
}

func RemoveNotification(user int, pattern string) {
	db.Lock()
	defer db.Unlock()
	if _, ok := db.m[user]; !ok {
		db.m[user] = userPreferences{map[string]bool{}, map[string]bool{}}
	}
	delete(db.m[user].notifications, pattern)
}

func GetNotifications(user int) []string {
	db.Lock()
	defer db.Unlock()
	notifications := make([]string, 0, 10)
	if _, ok := db.m[user]; !ok {
		return notifications
	}
	for pattern, _ := range db.m[user].notifications {
		notifications = append(notifications, pattern)
	}
	return notifications
}

func Users() []int {
	db.Lock()
	defer db.Unlock()
	keys := make([]int, 0, len(db.m))
	for user, _ := range db.m {
		keys = append(keys, user)
	}
	return keys
}
