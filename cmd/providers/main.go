package main

import (
	"fmt"
	"gitlab.com/y3g0r/go-onair/db"
	"gitlab.com/y3g0r/go-onair/providers"
	"gitlab.com/y3g0r/go-onair/providers/crawler"
	"gitlab.com/y3g0r/go-onair/tgbot"
	"log"
	"net/http"
	"os"
	"strconv"
	"strings"
	"time"
)

type CrawlResult struct {
	air *crawler.AirInfo
	err error
}

func main() {
	myid, err := strconv.Atoi(os.Getenv("ADMIN"))
	if err != nil {
		log.Panic(err)
	}

	db.InitDB()
	taskQ := make(chan string)
	resultsCh := workerPool(taskQ, 10)

	go crawlRadiosPeriodically(taskQ)
	go notifier(resultsCh, myid)

	http.HandleFunc("/demo", tgbot.TelegramWebhookHander)
	log.Fatal(http.ListenAndServe(":8080", nil))
}

func workerPool(taskQ <-chan string, numWorkers int) chan CrawlResult {
	resultsQ := make(chan CrawlResult)
	for i := 0; i < numWorkers; i++ {
		log.Printf("Starting worker %d\n", i)
		go worker(taskQ, resultsQ)
	}
	return resultsQ
}

func worker(taskQ <-chan string, taskDone chan<- CrawlResult) {
	for {
		stationName := <-taskQ
		onair, err := whatsOnAir(stationName)
		if err != nil {
			taskDone <- CrawlResult{nil, fmt.Errorf("%s: %v\n", stationName, err)}
		} else {
			taskDone <- CrawlResult{onair, err}
		}
	}
}

func whatsOnAir(stationName string) (*crawler.AirInfo, error) {
	var err error
	var onair *crawler.AirInfo
	provider := providers.Stations[stationName]
	onair, err = provider.Crawl()
	if err == nil {
		return onair, nil
	}
	return nil, err
}

func crawlRadiosPeriodically(taskQ chan string) {
	for {
		for stationName, provider := range providers.Stations {
			if provider.Disabled() {
				continue
			}
			taskQ <- stationName
		}
		time.Sleep(time.Minute)
	}
}

func notifier(resultsCh chan CrawlResult, myid int) {
	for res := range resultsCh {
		var msg string
		if res.err != nil {
			msg = fmt.Sprintf("%v\n", res.err)
			fmt.Print(msg)
			go tgbot.SendMessage(myid, msg)
			continue
		}
		playing := fmt.Sprintf("%s - %s", strings.Join(res.air.Artists, " & "), res.air.Song)
		msg = fmt.Sprintf("%s: %s\n", res.air.Radio, playing)
		fmt.Printf("[%v] %s\n", res.air.Time, msg)

		playingLower := strings.ToLower(playing)
		for _, user := range db.Users() {
			for _, notify := range db.GetNotifications(user) {
				if strings.Contains(playingLower, notify) {
					go tgbot.SendMessage(user, msg)
				}
			}
		}
	}
}
